﻿using System;
using System.Text;

namespace Lab2
{
    class Program
    {
        // Checks if string 1 contains all the characters of string 2
        static bool CheckThatString1ContainsAllCharactersOfString2(string inpStr1, string inpStr2)
        {
            int inpStr1Length = inpStr1.Length;
            int inpStr2Length = inpStr2.Length;

            bool containCharacter = false;

            for(int i = 0; i < inpStr2Length; i++)
            {
                containCharacter = false;

                for (int j = 0; j < inpStr1Length; j++)
                {
                    if(inpStr2[i] == inpStr1[j])
                    {
                        containCharacter = true;
                        break;
                    }
                }

                if(!containCharacter)
                {
                    break;
                }
            }

            return containCharacter;
        }

        // Replace all characters in Str1 equal to the characters from Str2 with a input character
        static string ReplaceAllCharactersInStr1EqualToCharactersFromStr2WithACharacter(string inpStr1, string inpStr2, char symbol)
        {
            string resultString;

            StringBuilder str = new StringBuilder(inpStr1);

            int inpStr1Length = inpStr1.Length;
            int inpStr2Length = inpStr2.Length;

            for (int i = 0; i < inpStr2Length; i++)
            {

                for (int j = 0; j < inpStr1Length; j++)
                {
                    if (inpStr2[i] == inpStr1[j])
                    {
                        str[j] = symbol;
                    }
                }
            }

            resultString = str.ToString();

            return resultString;
        }

        // Count how many times Str2 occurs in Str1
        static int CountHowManyTimesStr2OccursInStr1(string inpStr1, string inpStr2)
        {
            int counterString = 0;
            int positionStr2 = 0;

            int inpStr1Length = inpStr1.Length;
            int inpStr2Length = inpStr2.Length;

            if(inpStr2Length > inpStr1Length)
            {
                counterString = 0;

                return counterString;
            }

            for (int i = 0; i < inpStr1Length; i++)
            {
                if(inpStr2[positionStr2] == inpStr1[i])
                {
                    positionStr2++;
                }
                else
                {
                    positionStr2 = 0;

                    if(inpStr2[0] == inpStr1[i])
                    {
                        positionStr2++;
                    }
                }

                if(positionStr2 == inpStr2Length)
                {
                    counterString++;
                    i = i -  positionStr2 + 1;
                    positionStr2 = 0;
                }
            }

            return counterString;
        }

        static void Main(string[] args)
        {
            string str1;
            string str2;

            Console.WriteLine("--------****Lab2****--------");
            Console.Write("Enter string 1: ");
            str1 = Console.ReadLine();
            Console.Write("Enter string 2: ");
            str2 = Console.ReadLine();

            Console.WriteLine($"1. Checks if string 1 contains all the characters of string 2: \t\t\t\t\t{CheckThatString1ContainsAllCharactersOfString2(str1, str2)}");

            Console.Write("2. Replace all characters in Str1 equal to the characters from Str2 with a input character:\t");
            Console.WriteLine(ReplaceAllCharactersInStr1EqualToCharactersFromStr2WithACharacter(str1, str2, '#'));

            Console.WriteLine($"3. Count how many times Str2 occurs in Str1:\t\t\t\t\t\t\t{CountHowManyTimesStr2OccursInStr1(str1, str2)}");
        }
    }
}
