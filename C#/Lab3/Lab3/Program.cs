﻿using System;
using System.Collections.Generic;

namespace Lab3
{
    class Program
    {
        // Print 2D char array
        static void Print2DArray(char[,] array, int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write($"{array[i, j]}\t");
                }

                Console.WriteLine("\n");
            }
        }

        static char GetRandom(int typeRandom, int range1, int range2)
        {
            Random random = new Random();

            int randomNumber = random.Next(range1, range2); ;
            char randomChar = ' ';

            if(typeRandom == 1)
            {
                randomNumber = random.Next(1, 10);
                randomChar = Convert.ToChar(randomNumber.ToString());
            }
            else if (typeRandom == 2)
            {
                if (randomNumber % 2 == 0)
                {
                    randomChar = Convert.ToChar(randomNumber.ToString());
                }
                else
                {
                    return GetRandom(typeRandom, range1, range2);
                }
            }
            else if (typeRandom == 3)
            {
                if (randomNumber % 2 != 0)
                {
                    randomChar = Convert.ToChar(randomNumber.ToString());
                }
                else
                {
                    return GetRandom(typeRandom, range1, range2);
                }
            }
            else if (typeRandom == 4)
            {
                randomChar = (char)(33 + random.Next(14));
            }
            else if (typeRandom == 5)
            {
                randomChar = (char)(97 + random.Next(26));
            }
            
            return randomChar;
        }

        delegate char RandomDelegate(int typeRandom, int range1, int range2);

        // Transpose the char matrix
        static char[,] TransposeMatrix(char[,] array, int size)
        {
            char[,] resultTransposeMatrix = CopyArray(array, size);
            char temp;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    temp = resultTransposeMatrix[i, j];
                    resultTransposeMatrix[i, j] = resultTransposeMatrix[j, i];
                    resultTransposeMatrix[j, i] = temp;
                }
            }
            return resultTransposeMatrix;
        }

        // Cope char array
        static char[,] CopyArray(char[,] array, int size)
        {
            char[,] resultReflectMatrix = new char[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    resultReflectMatrix[i, j] = array[i, j];
                }
            }

            return resultReflectMatrix;
        }

        //Optional Area, options(1. fill 2. max min 3. search symbol 4. Rewrite Area), typeArea from 1 to 14, typeRamdom from 0 to 4
        static char[,] OptionalArea(char[,] array, int size, int typeArea, int option, RandomDelegate typeDelegate, int typeRandom, int range1, int range2, char symbol)
        {
            char[,] resultArray = CopyArray(array, size);
            List<char> rewriteList = new List<char>();

            int countRewrite = 0;
            int maxNumber = Int32.MinValue;
            int minNumber = Int32.MaxValue;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (typeArea == 1)
                    {
                        if (i >= j)
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 2)
                    {
                        if (i <= j)
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 3)
                    {
                        if (i + j <= size - 1)
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 4)
                    {
                        if (i + j >= size - 1)
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 5)
                    {
                        if (((i + j <= size - 1) && (i <= j)) || ((i + j >= size - 1) && (i >= j)))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 6)
                    {
                        if (((i + j >= size - 1) && (i <= j)) || ((i + j <= size - 1) && (i >= j)))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 7)
                    {
                        if ((i + j <= size - 1) && (i >= j))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 8)
                    {
                        if ((i + j >= size - 1) && (i <= j))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 9)
                    {
                        if ((i + j >= size - 1) && (i >= j))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 10)
                    {
                        if ((i + j <= size - 1) && (i <= j))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 11)
                    {
                        if ((i >= j) || ((i + j <= size - 1) && (i <= j)))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 12)
                    {
                        if ((i <= j) || ((i + j >= size - 1) && (i >= j)))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 13)
                    {
                        if ((i <= j) || ((i + j <= size - 1) && (i >= j)))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                    else if (typeArea == 14)
                    {
                        if ((i >= j) || ((i + j >= size - 1) && (i <= j)))
                        {
                            if (option == 1)
                            {
                                resultArray[i, j] = typeDelegate(typeRandom, range1, range2);
                            }
                            else if (option == 2)
                            {
                                if (array[i, j] > maxNumber)
                                {
                                    maxNumber = array[i, j];
                                }

                                if (array[i, j] < minNumber)
                                {
                                    minNumber = array[i, j];
                                }
                            }
                            else if (option == 3)
                            {
                                if (array[i, j] == symbol)
                                {
                                    Console.WriteLine($"Array[{i}, {j}] = {array[i, j]}");
                                }
                            }
                            else if (option == 4)
                            {
                                rewriteList.Add(resultArray[i, j]);
                            }
                        }
                    }
                }
            }

            if (option == 2)
            {
                Console.WriteLine($"Min = {Convert.ToChar(minNumber)}, Max = {Convert.ToChar(maxNumber)}");
            }
            else if (option == 4)
            {
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        if (typeArea == 2)
                        {
                            if (i >= j)
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 1)
                        {
                            if (i <= j)
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 4)
                        {
                            if (i + j <= size - 1)
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 3)
                        {
                            if (i + j >= size - 1)
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 6)
                        {
                            if (((i + j <= size - 1) && (i <= j)) || ((i + j >= size - 1) && (i >= j)))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 5)
                        {
                            if (((i + j >= size - 1) && (i <= j)) || ((i + j <= size - 1) && (i >= j)))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 8)
                        {
                            if ((i + j <= size - 1) && (i >= j))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 7)
                        {
                            if ((i + j >= size - 1) && (i <= j))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 10)
                        {
                            if ((i + j >= size - 1) && (i >= j))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 9)
                        {
                            if ((i + j <= size - 1) && (i <= j))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 12)
                        {
                            if ((i >= j) || ((i + j <= size - 1) && (i <= j)))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 11)
                        {
                            if ((i <= j) || ((i + j >= size - 1) && (i >= j)))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 14)
                        {
                            if ((i <= j) || ((i + j <= size - 1) && (i >= j)))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                        else if (typeArea == 13)
                        {
                            if ((i >= j) || ((i + j >= size - 1) && (i <= j)))
                            {
                                resultArray[i, j] = rewriteList[countRewrite++];
                            }
                        }
                    }
                }
            }

            return resultArray;
        }

        static void Main(string[] args)
        {
           Console.WriteLine("-----------****Lab3****-----------");

            const int _typeArray = 14;
            const int _randomType = 5;

            char searchSymbol;
            int _sizeArray;
            int range1;
            int range2;

            RandomDelegate firstType;
            firstType = GetRandom;

            Console.Write("Enter size array: ");
            _sizeArray = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter search char symbol: ");
            searchSymbol = Convert.ToChar(Console.ReadLine());

            Console.Write("Enter random int range1: ");
            range1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter random int range2: ");
            range2 = Convert.ToInt32(Console.ReadLine());

            char[,] array = new char[_sizeArray, _sizeArray];
            char[,] tempArray = new char[_sizeArray, _sizeArray];

            for (int i = 1; i < _typeArray + 1; i++)
            {
                Console.WriteLine($"<==================================<=Area #{i}=>==================================>");

                for (int j = 1; j < _randomType + 1; j++)
                {
                    Console.WriteLine($"<==============================<=Random type #{j}=>===============================>");
                    Console.WriteLine($"<=Fill area=>\n");
                    tempArray = CopyArray(OptionalArea(array, _sizeArray, i, 1, firstType, j, range1, range2, searchSymbol), _sizeArray);
                    Print2DArray(tempArray, _sizeArray);

                    Console.WriteLine($"\n<=Search min, max=>\n");
                    if (j == 1 || j == 2 || j == 3)
                    {
                        OptionalArea(tempArray, _sizeArray, i, 2, firstType, j, range1, range2, searchSymbol);
                    }

                    Console.WriteLine($"\n<=Search symbol \"{searchSymbol}\"=>\n");
                    OptionalArea(tempArray, _sizeArray, i, 3, firstType, j, range1, range2, searchSymbol);

                    Console.WriteLine($"\n<=Transpose=>\n");
                    Print2DArray(TransposeMatrix(tempArray, _sizeArray), _sizeArray);

                    Console.WriteLine($"\n<=Rewrite Area=>\n");
                    Print2DArray(OptionalArea(tempArray, _sizeArray, i, 4, firstType, j, range1, range2, searchSymbol), _sizeArray);
                }
            }
        }
    }
}