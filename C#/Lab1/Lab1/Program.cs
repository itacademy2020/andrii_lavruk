﻿using System;
using System.Numerics;

namespace Lab1
{
    class Program
    {
        // Calls up the program menu
        static void Menu()
        {
            string MenuVariant;
            bool MenuActivator = true;
            bool ReInputActivator = true;

            do
            {
                Console.WriteLine();
                Console.WriteLine("*************** Menu ***************");
                Console.WriteLine("1. The number of digits in the number N.");
                Console.WriteLine("2. An array whose elements are the digits of the number N.");
                Console.WriteLine("3. Arithmetic mean of the digits of N.");
                Console.WriteLine("4. Geometric mean of digits N.");
                Console.WriteLine("5. Factorial of number N");
                Console.WriteLine("6. The sum of all paired numbers from 1 to N.");
                Console.WriteLine("7. The sum of all unpaired numbers from 1 to N.");
                Console.WriteLine("8. Overloading of functions 7 and 8, parameters set the range of values in which to find the sum.");
                Console.WriteLine("0. Exit.");
                Console.WriteLine();

                do
                {
                    ReInputActivator = false;

                    Console.Write("Select a menu item: ");
                    MenuVariant = Console.ReadLine();

                    switch (MenuVariant)
                    {
                        case "1":
                            Console.WriteLine("\nYou select: \"1.The number of digits in the number N.\"");
                            Console.WriteLine($"Digits in input number: {NumberOfDgitsInNumber(InputInteger())}.");
                            break;
                        case "2":
                            Console.WriteLine("\nYou select: \"2. An array whose elements are the digits of the number N.\"");
                            int[] newArrey = CreateArrayWhoseElementsAreDigitsOfNumber(InpuPositiveInteger());
                            Console.WriteLine("Array of the digits of input number: ");
                            PrintArray1D(newArrey);
                            break;
                        case "3":
                            Console.WriteLine("\nYou select: \"3. Arithmetic mean of the digits of N.\"");
                            Console.WriteLine($"Arithmetic mean of the digits of input number: {ArithmeticMeanOfDigitsOfNumber(InpuPositiveInteger())}.");
                            break;
                        case "4":
                            Console.WriteLine("\nYou select: \"4. Geometric mean of digits N.\"");
                            Console.WriteLine($"Geometric mean of the digits of input number: {GeometricMeanOfDigitsOfNumber(InpuPositiveInteger())}.");
                            break;
                        case "5":
                            Console.WriteLine("\nYou select: \"5. Factorial of number N\"");
                            Console.WriteLine($"Factorial of input number: {Factorial(InpuPositiveInteger())}.");
                            break;
                        case "6":
                            Console.WriteLine("\nYou select: \"6. The sum of all paired numbers from 1 to N.\"");
                            Console.WriteLine($"The sum of all even numbers from 1 to InputNumber: {SumOfAllEvenNumbersFrom1ToInputNumber(InpuPositiveInteger())}.");
                            break;
                        case "7":
                            Console.WriteLine("\nYou select: \"7. The sum of all unpaired numbers from 1 to N.\"");
                            Console.WriteLine($"The sum of all odd numbers from 1 to InputNumber: {SumOfAllOddNumbersFrom1ToInputNumber(InpuPositiveInteger())}.");
                            break;
                        case "8":
                            Console.WriteLine("\nYou select: \"8. Overloading of functions 7 and 8, parameters set the range of values in which to find the sum.\"");
                            int tempRange1, tempRange2;

                            Console.Write("Range1 -> ");
                            tempRange1 = InputInteger();
                            Console.Write("Range2 -> ");
                            tempRange2 = InputInteger();

                            Console.WriteLine($"The sum of all even numbers from {tempRange1} to {tempRange2}: {SumOfAllEvenNumbersFrom1ToInputNumber(tempRange1, tempRange2)}.");
                            Console.WriteLine($"The sum of all odd numbers from {tempRange1} to {tempRange2}: {SumOfAllOddNumbersFrom1ToInputNumber(tempRange1, tempRange2)}.");
                            break;
                        case "0":
                            MenuActivator = false;
                            Console.WriteLine("Exit.");
                            break;
                        default:
                            Console.WriteLine("Input error.");
                            ReInputActivator = true;
                            break;
                    }
                }
                while (ReInputActivator);

                Console.ReadKey();
            }
            while (MenuActivator);
        }

        // Checks whether the term can be converted to an integer
        static bool CheckForIntegers(string inpNumInString)
        {
            try
            {
                int newInteger = Convert.ToInt32(inpNumInString);
            }
            catch
            {
                return false;
            }

            return true;
        }

        // Prompts for an integer, checks for an integer, and returns an integer
        static int InputInteger()
        {
            string inpStr;
            int integerFromtString = 0;
            bool reinputvalue = true;

            while (reinputvalue)
            {
                Console.WriteLine("Enter an integer: ");
                inpStr = Console.ReadLine();

                if (CheckForIntegers(inpStr))
                {
                    integerFromtString = Convert.ToInt32(inpStr);
                    reinputvalue = false;
                }
                else
                {
                    Console.WriteLine("Input error.");
                }
            }

            return integerFromtString;
        }

        // Prompts for an positive integer, checks for an integer, and returns an integer
        static int InpuPositiveInteger()
        {
            return IntegerModule(InputInteger());
        }

        // Returns the number of digits in a number
        static int NumberOfDgitsInNumber(int inpNumper)
        {
            int DigitsInNumber = 0;
            int AbsInpNumper = IntegerModule(inpNumper);

            if (inpNumper == 0)
            {
                DigitsInNumber++;
            }
            else
            {
                while (AbsInpNumper > 0)
                {
                    DigitsInNumber++;

                    AbsInpNumper = AbsInpNumper / 10;
                }
            }

            return DigitsInNumber;
        }

        // Takes an integer and creates an array of the digits of that number
        static int[] CreateArrayWhoseElementsAreDigitsOfNumber(int inpNumper)
        {
            int DigitsInNumber = NumberOfDgitsInNumber(inpNumper);

            int[] outArray = new int[DigitsInNumber];

            string inpNumberToString = inpNumper.ToString();
            string tempString;

            for(int i = 0; i < DigitsInNumber; i++)
            {
                tempString = inpNumberToString[i].ToString();

                outArray[i] = Convert.ToInt32(tempString);
            }

            return outArray;
        }

        // Displays the contents of the array on the console
        static void PrintArray1D(int[] inpArray)
        {
            foreach (int i in inpArray)
            {
                Console.Write(i);
                Console.Write("\t");
            }
            Console.WriteLine();
        }

        // Takes the modulus of an integer
        static int IntegerModule(int inpNumber)
        {
            int resultIntegerModule;

            if (inpNumber < 0)
            {
                resultIntegerModule = inpNumber * (-1);
            }
            else
            {
                resultIntegerModule = inpNumber;
            }

            return resultIntegerModule;
        }

        // Displays the arithmetic mean of the digits of the number
        static double ArithmeticMeanOfDigitsOfNumber(int inpNumber)
        {
            int[] tempArrey = CreateArrayWhoseElementsAreDigitsOfNumber(inpNumber);
            int arrayLength = tempArrey.Length;
            double result;

            double sumOfAllDigitsOfInputNumber = 0;

            for (int i = 0; i < arrayLength; i++)
            {
                sumOfAllDigitsOfInputNumber += tempArrey[i];
            }

            result = sumOfAllDigitsOfInputNumber / arrayLength;

            return result;
        }

        // Displays the geometric mean of the digits of the number
        static double GeometricMeanOfDigitsOfNumber(int inpNumber)
        {
            int[] tempArrey = CreateArrayWhoseElementsAreDigitsOfNumber(inpNumber);
            int arrayLength = tempArrey.Length;
            double result;

            double productOfAllDigitsOfInputNumber = 1;

            for (int i = 0; i < arrayLength; i++)
            {
                productOfAllDigitsOfInputNumber *= tempArrey[i];
            }

            result = Math.Pow(productOfAllDigitsOfInputNumber, 1.0 / arrayLength);

            return result;
        }

        // Factorial of number
        static BigInteger Factorial(int inpNumber)
        {
            if (inpNumber == 0)
            {
                return 1;
            }
            else
            {
                return (BigInteger)(inpNumber * Factorial(inpNumber - 1));
            }
        }

        // Returns the sum of all even numbers from 1 to InputNumber
        static int SumOfAllEvenNumbersFrom1ToInputNumber(int inpNumber)
        {
            int sumOfAllEvenNumbers = 0;

            for (int i = 1; i < inpNumber + 1; i++)
            {
                if((i % 2) == 0)
                {
                    sumOfAllEvenNumbers += i;
                }
            }

            return sumOfAllEvenNumbers;
        }

        // Returns the sum of all odd numbers from 1 to InputNumber
        static int SumOfAllOddNumbersFrom1ToInputNumber(int inpNumber)
        {
            int sumOfAllOddNumbers = 0;

            for (int i = 1; i < inpNumber + 1; i++)
            {
                if ((i % 2) != 0)
                {
                    sumOfAllOddNumbers += i;
                }
            }

            return sumOfAllOddNumbers;
        }

        // Returns the sum of all even numbers from range1 to range2
        static int SumOfAllEvenNumbersFrom1ToInputNumber(int range1, int range2)
        {
            int sumOfAllEvenNumbers = 0;

            for (int i = range1; i < range2 + 1; i++)
            {
                if ((i % 2) == 0)
                {
                    sumOfAllEvenNumbers += i;
                }
            }

            return sumOfAllEvenNumbers;
        }

        // Returns the sum of all odd numbers from range1 to range2
        static int SumOfAllOddNumbersFrom1ToInputNumber(int range1, int range2)
        {
            int sumOfAllOddNumbers = 0;

            for (int i = range1; i < range2 + 1; i++)
            {
                if ((i % 2) != 0)
                {
                    sumOfAllOddNumbers += i;
                }
            }

            return sumOfAllOddNumbers;
        }

        static void Main(string[] args)
        {
            Menu();
        }
    }
}